package com.tom.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

@SpringBootApplication

@Service
public class CalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculatorApplication.class, args);
	}
	
	public static int addNumbers(int inputNum1, int inputNum2)
	{
		return inputNum1+inputNum2;
	}
	
	public static int subtractNumbers(int inputNum1, int inputNum2)
	{
		return inputNum1-inputNum2;
	}
	
	public static int multiNumbers(int inputNum1, int inputNum2)
	{
		return inputNum1*inputNum2;
	}
	
	public static int divideNumbers(int inputNum1, int inputNum2)
	{
		if(inputNum2==0)
		{
			System.out.println("Division by 0 is not allowed!");
			return 0; //error code
		}
		else
		return inputNum1/inputNum2;
	}
}
