package com.tom.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorApplicationTests {
@Autowired

	@Test
	public void testAddTwoNumbersShouldReturnPositiveResult() throws Exception 
    {
		int result=CalculatorApplication.addNumbers(1,2);
		assertEquals(3, result);
	}

	@Test
	public void testSubtractTWoPositiveNumbers() throws Exception
	{
		int result=CalculatorApplication.subtractNumbers(2,1);
		assertEquals(1, result);
	}
	
	@Test
	public void testMultiplyTwoPositiveNumbers() throws Exception
	{
		int result=CalculatorApplication.multiNumbers(2,2);
		assertEquals(4, result);
	}
	
	@Test
	public void testDividingTwoPositiveNumbers() throws Exception
	{
		int result=CalculatorApplication.divideNumbers(4,2);
		assertEquals(2, result);
	}
	
	@Test
	public void testDividingTwoNumbersbyZero() throws Exception
	{
		int result=CalculatorApplication.divideNumbers(4,0);
		assertEquals(0, result);
	}
}
